import axios from "axios";

const CONTENTFUL_API_URL =
  "https://cdn.contentful.com/spaces/vveq832fsd73/entries";
const CONTENTFUL_API_KEY = "VPmo2U661gTnhMVx0pc0-CtahNg_aqS5DuneLtYfO1o";

export const listDiscounts = async (): Promise<any> => {
  try {
    const response = await axios.get(
      `${CONTENTFUL_API_URL}?content_type=discount`,
      {
        headers: {
          Authorization: `Bearer ${CONTENTFUL_API_KEY}`,
        },
      }
    );

    return response.data;
  } catch (error) {
    throw new Error("Failed to fetch car data from Contentful");
  }
};
