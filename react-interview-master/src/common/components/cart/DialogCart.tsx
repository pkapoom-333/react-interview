import React, { useEffect, useState, useCallback } from "react";
import { useContextSelector } from "use-context-selector";
import {
  Dialog,
  DialogTitle,
  IconButton,
  DialogContent,
  DialogActions,
  Grid,
  CardMedia,
  ListItemText,
  Typography,
  Box,
} from "@mui/material";
import CloseIcon from "@mui/icons-material/Close";
import AddIcon from "@mui/icons-material/Add";
import RemoveIcon from "@mui/icons-material/Remove";
import { CustomTextField } from "../custom/textfield";
import { CartContext } from "../../../context/CartContext";
import { listDiscounts } from "../../../service/DiscountService";

interface TDialogCart {
  handleClose: () => void;
}

export const DialogCart = ({ handleClose }: TDialogCart) => {
  const [listDataDiscounts, setListDataDiscounts] = useState<any[]>([]);
  const dispatch = useContextSelector(CartContext, (v) => v[1]);
  const listCartItems = useContextSelector(CartContext, (v) => v[0].cartItems);
  const total = useContextSelector(CartContext, (v) => v[0].total);
  const discount = useContextSelector(CartContext, (v) => v[0].discount);
  const grandTotal = useContextSelector(CartContext, (v) => v[0].grandTotal);
  const code = useContextSelector(CartContext, (v) => v[0].discountCode);
  const [discountCode, setDiscountCode] = useState(code);

  const handleChangeDiscountCode = (event: any) => {
    setDiscountCode(event.target.value);
  };

  const handleAddCart = (data: any) => {
    dispatch({ type: "ADD_Count_OF_CART", payload: { id: data.sys.id } });
  };

  const handleDeleteCart = (data: any) => {
    if (data.count <= 1) {
      dispatch({ type: "REMOVE_FROM_CART", payload: { id: data.sys.id } });
    }

    dispatch({ type: "DELETE_Count_OF_CART", payload: { id: data.sys.id } });
  };

  const getTotol = useCallback(() => {
    let totalPrice = 0;
    listCartItems.forEach(
      (item) => (totalPrice = totalPrice + item.fields.price * item.count)
    );
    dispatch({ type: "SET_TOTAL", payload: { total: totalPrice } });
    dispatch({
      type: "SET_GRAND_TOTAL",
      payload: {
        grandTotal: totalPrice > discount ? totalPrice - discount : 0,
      },
    });
  }, [listCartItems, dispatch, discount]);

  const findDiscount = useCallback(() => {
    if (listDataDiscounts?.length > 0) {
      const discountFields = listDataDiscounts.find(
        (item: any) =>
          item.fields.code.toUpperCase() === discountCode.toUpperCase()
      );

      if (discountFields) {
        dispatch({
          type: "SET_DISCOUNT",
          payload: { discount: discountFields.fields.amount },
        });
        dispatch({
          type: "SET_DISCOUNT_CODE",
          payload: { discountCode: discountCode },
        });
        dispatch({
          type: "SET_GRAND_TOTAL",
          payload: {
            grandTotal:
              total > discountFields.fields.amount
                ? total - discountFields.fields.amount
                : 0,
          },
        });
      } else {
        dispatch({
          type: "SET_DISCOUNT",
          payload: { discount: 0 },
        });
        dispatch({
          type: "SET_DISCOUNT_CODE",
          payload: { discountCode: "" },
        });

        dispatch({
          type: "SET_GRAND_TOTAL",
          payload: {
            grandTotal: total,
          },
        });
      }
    }
  }, [discountCode, dispatch, listDataDiscounts, total]);

  useEffect(() => {
    getTotol();
  }, [listCartItems, getTotol]);

  useEffect(() => {
    findDiscount();
  }, [discountCode, findDiscount]);

  useEffect(() => {
    listDiscounts()
      .then((discounts) => {
        if (discounts.items && discounts.items.length > 0) {
          setListDataDiscounts(discounts.items);
        }
      })
      .catch((error) => {
        console.error(error.message);
      });
  }, []);

  return (
    <Dialog
      onClose={handleClose}
      open={true}
      sx={{
        borderRadius: "16px",
        "& .MuiDialog-paper": {
          margin: "2px",
          width: "530px",
          padding: "0px",
        },
      }}
    >
      <DialogTitle sx={{ padding: "5px 10px" }} id="customized-dialog-title">
        <Typography
          sx={{
            fontFamily: "Inter, sans-serif",
            fontSize: "30px",
            fontWeight: 600,
            lineHeight: "36px",
          }}
          component="div"
        >
          {"Cart"}
        </Typography>
      </DialogTitle>
      <IconButton
        aria-label="close"
        onClick={handleClose}
        sx={{
          position: "absolute",
          right: 8,
          top: 8,
          color: (theme) => theme.palette.grey[500],
        }}
      >
        <CloseIcon />
      </IconButton>
      <DialogContent
        sx={{
          padding: "0px",
        }}
      >
        <Grid container>
          <Grid item xs={12}>
            <Box
              sx={{
                minHeight: "300px",
                maxHeight: "300px",
                overflowY: "auto",
              }}
            >
              {listCartItems.map((data: any) => {
                return (
                  <Grid
                    container
                    key={data.fields.title}
                    alignItems={"center"}
                    sx={{
                      borderBottom: "1px solid #D1D5DB",
                      paddingY: "10px",
                      paddingX: "10px",
                    }}
                  >
                    <Grid
                      item
                      lg={3}
                      md={3}
                      sm={3}
                      xs={false}
                      sx={{
                        display: {
                          lg: "block",
                          md: "block",
                          sm: "block",
                          xs: "none",
                        },
                        paddingRight: "10px",
                      }}
                    >
                      <Box
                        display={"flex"}
                        alignItems={"center"}
                        justifyContent={"center"}
                        height={"100%"}
                      >
                        <CardMedia
                          component="img"
                          height="54"
                          image={data.fields.photo}
                          alt="green iguana"
                          onError={(e: any) => {
                            e.target.src = require("../../../assets/BackgroudImg.png");
                          }}
                        />
                      </Box>
                    </Grid>
                    <Grid item lg={6} md={6} sm={6} xs={7}>
                      <ListItemText
                        primary={
                          <React.Fragment>
                            <Typography
                              sx={{
                                fontFamily: "Inter, sans-serif",
                                fontSize: "20px",
                                fontWeight: 700,
                                lineHeight: "28px",
                              }}
                              component="div"
                            >
                              {data.fields.title}
                            </Typography>
                          </React.Fragment>
                        }
                        secondary={
                          <React.Fragment>
                            <Typography
                              sx={{
                                fontFamily: "Inter, sans-serif",
                                fontSize: "14px",
                                fontWeight: 500,
                                lineHeight: "20px",
                              }}
                            >
                              {data.fields.price} {"THB/Day"}
                            </Typography>
                          </React.Fragment>
                        }
                      />
                    </Grid>
                    <Grid
                      item
                      lg={3}
                      md={3}
                      sm={3}
                      xs={5}
                      sx={{
                        paddingLeft: "10px",
                      }}
                    >
                      <Box
                        display={"flex"}
                        alignItems={"center"}
                        justifyContent={"center"}
                        width={"100%"}
                      >
                        <Box
                          width={"33.33%"}
                          display={"flex"}
                          alignItems={"center"}
                          justifyContent={"center"}
                        >
                          <IconButton
                            sx={{
                              width: "30px",
                              height: "30px",
                              background: "#3B82F6",
                              borderRadius: "8px",
                              color: "#FFF",
                            }}
                            onClick={() => handleAddCart(data)}
                            disableRipple
                            disableFocusRipple
                            disableTouchRipple
                          >
                            <AddIcon />
                          </IconButton>
                        </Box>
                        <Box
                          width={"33.33%"}
                          display={"flex"}
                          alignItems={"center"}
                          justifyContent={"center"}
                        >
                          <Typography
                            sx={{
                              fontFamily: "Inter, sans-serif",
                              fontSize: "24px",
                              fontWeight: 400,
                              lineHeight: "32px",
                            }}
                            component="div"
                          >
                            {data.count}
                          </Typography>
                        </Box>
                        <Box
                          width={"33.33%"}
                          display={"flex"}
                          alignItems={"center"}
                          justifyContent={"center"}
                          onClick={() => handleDeleteCart(data)}
                        >
                          <IconButton
                            sx={{
                              width: "30px",
                              height: "30px",
                              background: "#3B82F6",
                              borderRadius: "8px",
                              color: "#FFF",
                            }}
                            disableRipple
                            disableFocusRipple
                            disableTouchRipple
                          >
                            <RemoveIcon />
                          </IconButton>
                        </Box>
                      </Box>
                    </Grid>
                  </Grid>
                );
              })}
            </Box>
          </Grid>
          <Grid
            item
            xs={12}
            sx={{
              background: "#F3F4F6",
              height: "75px",
            }}
          >
            <Box
              sx={{
                display: "flex",
                alignItems: "center",
                paddingX: "15px",
                height: "75px",
              }}
            >
              <CustomTextField
                id="outlined-basic"
                variant="outlined"
                placeholder="Discount code"
                value={discountCode}
                onChange={handleChangeDiscountCode}
                fullWidth
              />
            </Box>
          </Grid>
        </Grid>
      </DialogContent>
      <DialogActions sx={{ padding: "5px 10px" }}>
        <Grid container>
          <Grid
            item
            xs={12}
            sx={{
              borderBottom: "1px solid #D1D5DB",
            }}
          >
            <Box
              display={"flex"}
              alignItems={"center"}
              justifyContent={"space-between"}
              height={"42px"}
            >
              <Typography
                sx={{
                  fontFamily: "Inter, sans-serif",
                  fontSize: "18px",
                  fontWeight: 700,
                  lineHeight: "28px",
                }}
                component="div"
              >
                {"Total"}
              </Typography>
              <Typography
                sx={{
                  fontFamily: "Inter, sans-serif",
                  fontSize: "18px",
                  fontWeight: 400,
                  lineHeight: "28px",
                }}
                component="div"
              >
                {total}
                {" THB"}
              </Typography>
            </Box>
          </Grid>
          <Grid
            item
            xs={12}
            sx={{
              borderBottom: "1px solid #D1D5DB",
            }}
          >
            <Box
              display={"flex"}
              alignItems={"center"}
              justifyContent={"space-between"}
              height={"42px"}
            >
              <Typography
                sx={{
                  fontFamily: "Inter, sans-serif",
                  fontSize: "18px",
                  fontWeight: 700,
                  lineHeight: "28px",
                }}
                component="div"
              >
                {"Discount"}
              </Typography>
              <Typography
                sx={{
                  fontFamily: "Inter, sans-serif",
                  fontSize: "18px",
                  fontWeight: 400,
                  lineHeight: "28px",
                }}
                component="div"
              >
                {discount}
                {" THB"}
              </Typography>
            </Box>
          </Grid>
          <Grid item xs={12}>
            <Box
              display={"flex"}
              alignItems={"center"}
              justifyContent={"space-between"}
              height={"42px"}
            >
              <Typography
                sx={{
                  fontFamily: "Inter, sans-serif",
                  fontSize: "18px",
                  fontWeight: 700,
                  lineHeight: "28px",
                }}
                component="div"
              >
                {"Grand Total"}
              </Typography>
              <Typography
                sx={{
                  fontFamily: "Inter, sans-serif",
                  fontSize: "18px",
                  fontWeight: 400,
                  lineHeight: "28px",
                }}
                component="div"
              >
                {grandTotal}
                {" THB"}
              </Typography>
            </Box>
          </Grid>
        </Grid>
      </DialogActions>
    </Dialog>
  );
};
