import React, { useState } from "react";
import { useContextSelector } from "use-context-selector";
import { Box, AppBar, Toolbar, Typography, Badge } from "@mui/material";
import ShoppingIcon from "../../../assets/Shopping.svg";
import { CartContext } from "../../../context/CartContext";
import { DialogCart } from "../cart/DialogCart";

export const AppHead = () => {
  const listCartItems = useContextSelector(CartContext, (v) => v[0].cartItems);
  const [isOpenDialogCart, setIsOpenDialogCart] = useState(false);

  const handleOpenDialogCart = () => {
    setIsOpenDialogCart(true);
  };

  const handleCloseDialogCart = () => {
    setIsOpenDialogCart(false);
  };

  return (
    <Box>
      <AppBar
        component="nav"
        sx={{
          padding: {
            lg: "0px 100px",
            md: "0px 100px",
            sm: "0px 80px",
            xs: "0px 15px",
          },
          background: "#FFF",
          boxShadow: [
            "0px 4px 6px -2px rgba(0, 0, 0, 0.05)",
            "0px 10px 15px -3px rgba(0, 0, 0, 0.1)",
          ].join(","),
        }}
      >
        <Toolbar sx={{ paddingX: "0px !important", paddingRight: "18px" }}>
          <Box sx={{ flexGrow: 1 }}>
            <img alt="logo" src={require("../../../assets/Logo.jpg")} />
          </Box>
          <Box
            display={"flex"}
            flexDirection={"row"}
            alignItems={"center"}
            justifyContent={"center"}
            onClick={handleOpenDialogCart}
          >
            <Box
              display={"flex"}
              alignItems={"center"}
              justifyContent={"center"}
              width={"28px"}
              height={"28px"}
            >
              <Badge
                color="error"
                overlap="circular"
                badgeContent=" "
                variant="dot"
                invisible={listCartItems.length > 0 ? false : true}
                anchorOrigin={{
                  vertical: "top",
                  horizontal: "right",
                }}
                sx={{ width: "22px", height: "22px" }}
              >
                <img
                  alt="logo"
                  src={ShoppingIcon}
                  style={{ width: "24px", height: "24px", marginRight: "8px" }}
                />
              </Badge>
            </Box>
            <Typography
              sx={{
                fontFamily: "Inter, sans-serif",
                fontSize: "14px",
                fontWeight: 600,
                lineHeight: "20px",
                color: "rgba(17, 24, 39, 1)",
              }}
            >
              Cart({listCartItems.length})
            </Typography>
          </Box>
        </Toolbar>
      </AppBar>
      {isOpenDialogCart && <DialogCart handleClose={handleCloseDialogCart} />}
    </Box>
  );
};
