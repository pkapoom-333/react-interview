import React from "react";
import TextField, { TextFieldProps } from "@mui/material/TextField";

export const CustomTextField: React.FC<TextFieldProps> = ({ ...rest }) => {
  return (
    <TextField
      {...rest}
      sx={{
        "& .MuiOutlinedInput-input": {
          height: "33px", // Set the desired height
          padding: "5px 10px", // Adjust padding if needed
          // Add more custom styles as needed
        },
      }}
    />
  );
};
