import React from "react";
import { FormControl, Select, MenuItem, SelectProps, Box } from "@mui/material";

interface CustomSelectProps extends SelectProps {
  options: Array<{ value: any; label: string }>;
  startIcon?: React.ReactNode;
}

export const CustomSelect: React.FC<CustomSelectProps> = ({
  options,
  startIcon,
  ...rest
}) => {
  return (
    <FormControl fullWidth>
      <Select
        {...rest}
        sx={{
          "& .MuiSelect-select.MuiInputBase-input.MuiOutlinedInput-input.MuiSelect-select":
            {
              height: "23px",
              padding: "10px", // Set the desired height here
            },
        }}
      >
        {options.map((option) => (
          <MenuItem key={option.value} value={option.value}>
            <Box display="flex" alignItems="center">
              {startIcon} {option.label}
            </Box>
          </MenuItem>
        ))}
      </Select>
    </FormControl>
  );
};
