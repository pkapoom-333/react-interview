import React from "react";
import { Box, Typography, Grid } from "@mui/material";

export const Footer = () => {
  return (
    <Box
      sx={{
        position: "fixed",
        bottom: 0,
        left: 0,
        width: "100%",
        background: "rgba(17, 24, 39, 1)",
      }}
    >
      <Grid
        container
        sx={{
          padding: {
            lg: "40px 100px",
            md: "40px 100px",
            sm: "20px 80px",
            xs: "15px 15px",
          },
        }}
      >
        <Grid item lg={6} md={6} sm={6} xs={12}>
          <Typography
            sx={{
              fontFamily: "Inter, sans-serif",
              fontSize: "16px",
              fontWeight: 600,
              lineHeight: "24px",
              color: "#FFFFFF",
            }}
          >
            Drivehub Co., Ltd
          </Typography>
          <Typography
            sx={{
              marginTop: "10px",
              maxWidth: "250px",
              fontFamily: "Inter, sans-serif",
              fontSize: "12px",
              fontWeight: 400,
              lineHeight: "16px",
              color: "#FFFFFF",
            }}
          >
            193-195 Lake Rajada Office Complex, Ratchadapisek Road, Khlong Toei,
            Bangkok
          </Typography>
        </Grid>
        <Grid item lg={6} md={6} sm={6} xs={12}>
          <Box
            sx={{
              display: "flex",
              marginTop: { lg: 0, md: 0, sm: 0, xs: "10px" },
              alignItems: { lg: "end", md: "end", sm: "end", xs: "center" },
              justifyContent: { lg: "end", md: "end", sm: "end", xs: "start" },
              height: "100%",
            }}
          >
            <Typography
              component={"span"}
              sx={{
                maxWidth: "250px",
                fontFamily: "Inter, sans-serif",
                fontSize: "12px",
                fontWeight: 400,
                lineHeight: "16px",
                color: "#FFFFFF",
              }}
            >
              © Drivehub 2023
            </Typography>
          </Box>
        </Grid>
      </Grid>
    </Box>
  );
};
