import React, { useState, useEffect } from "react";
import { useContextSelector } from "use-context-selector";
import {
  Grid,
  Typography,
  Card,
  CardContent,
  CardMedia,
  CardActionArea,
  CardActions,
  Button,
} from "@mui/material";
import SwapVertIcon from "@mui/icons-material/SwapVert";
import { CartContext } from "../context/CartContext";
import { CustomTextField, CustomSelect } from "../common/components/custom";
import { listCars } from "../service/ContentfulService";

export const Home = () => {
  const dispatch = useContextSelector(CartContext, (v) => v[1]);
  const listCartItems = useContextSelector(CartContext, (v) => v[0].cartItems);
  const [sort, setSort] = useState("fields.price");
  const options = [
    { value: "fields.price", label: "Price: Low - High" },
    { value: "-fields.price", label: "Price: High - Low" },
  ];
  const [search, setSearch] = useState("");
  const [listData, setListData] = useState<any[]>();

  const handleChangeSearch = (event: any) => {
    setSearch(event.target.value);
  };

  const handleChangeSort = (event: any) => {
    setSort(event.target.value);
  };

  const handleAddtoCart = (data: any) => {
    dispatch({ type: "ADD_TO_CART", payload: { ...data, count: 1 } });
  };

  useEffect(() => {
    // Fetch car data when the component mounts
    listCars()
      .then((carData) => {
        if (carData.items) {
          const sortedData = carData.items.sort((a: any, b: any) => {
            const priceA = parseFloat(a.fields.price);
            const priceB = parseFloat(b.fields.price);

            if (sort === "fields.price") {
              return priceA - priceB; // Ascending order
            } else {
              return priceB - priceA; // Descending order
            }
          });

          // Filter the data by title
          const filteredData = sortedData.filter((item: any) =>
            item.fields.title.toLowerCase().includes(search.toLowerCase())
          );

          setListData(filteredData);
        }
      })
      .catch((error) => {
        console.error(error.message);
      });
  }, [search, sort]);

  return (
    <Grid container>
      <Grid
        item
        xs={12}
        sx={{
          padding: {
            lg: "0px 100px",
            md: "0px 100px",
            sm: "0px 80px",
            xs: "0px 15px",
          },
        }}
      >
        <Grid container>
          <Grid
            item
            md={6}
            sm={6}
            xs={12}
            sx={{
              padding: {
                lg: "20px 0px",
                md: "20px 0px",
                sm: "20px 0px",
                xs: "5px 0px",
              },
            }}
          >
            <Typography
              sx={{
                fontFamily: "Inter, sans-serif",
                fontSize: "30px",
                fontWeight: 600,
                lineHeight: "36px",
                color: "#111827",
              }}
            >
              {"Car Available"}
            </Typography>
          </Grid>
          <Grid
            item
            md={6}
            sm={6}
            xs={12}
            sx={{
              padding: {
                lg: "20px 0px",
                md: "20px 0px",
                sm: "20px 0px",
                xs: "5px 0px",
              },
            }}
          >
            <Grid container justifyContent={"flex-end"} spacing={2}>
              <Grid item lg={5.3} md={6} sm={7} xs={12}>
                <CustomTextField
                  id="outlined-basic"
                  variant="outlined"
                  placeholder="Search Car"
                  value={search}
                  onChange={handleChangeSearch}
                  fullWidth
                />
              </Grid>
              <Grid item lg={3.6} md={5} sm={5} xs={12}>
                <CustomSelect
                  value={sort}
                  onChange={handleChangeSort}
                  options={options}
                  displayEmpty
                  startIcon={<SwapVertIcon />}
                  inputProps={{
                    "aria-label": "Without label",
                  }}
                />
              </Grid>
            </Grid>
          </Grid>
        </Grid>
      </Grid>
      <Grid
        item
        xs={12}
        sx={{
          padding: {
            lg: "0px 100px",
            md: "0px 100px",
            sm: "0px 80px",
            xs: "0px 15px",
          },
          background: "#F3F4F6",
        }}
      >
        <Grid container spacing={3} marginTop={"10px"} marginBottom={"10px"}>
          {listData?.map((data) => {
            const isAlreadyInCart = listCartItems.some(
              (item: any) => item.sys.id === data.sys.id
            );

            return (
              <Grid item xs={12} sm={6} md={4} lg={3} key={data.fields.title}>
                <Card
                  sx={{
                    boxShadow:
                      "0px 2px 4px -1px rgba(0,0,0,0.06), 0px 4px 6px -2px rgba(0,0,0,0.1)",
                    borderRadius: "16px",
                  }}
                >
                  <CardActionArea disableRipple disableTouchRipple>
                    <CardMedia
                      component="img"
                      height="185"
                      image={data.fields.photo}
                      alt="green iguana"
                      onError={(e: any) => {
                        e.target.src = require("../assets/BackgroudImg.png");
                      }}
                    />
                    <CardContent sx={{ padding: "12px" }}>
                      <Typography
                        gutterBottom
                        sx={{
                          fontFamily: "Inter, sans-serif",
                          fontSize: "20px",
                          fontWeight: 700,
                          lineHeight: "28px",
                        }}
                        component="div"
                      >
                        {data.fields.title}
                      </Typography>
                      <Typography
                        sx={{
                          fontFamily: "Inter, sans-serif",
                          fontSize: "14px",
                          fontWeight: 500,
                          lineHeight: "20px",
                        }}
                      >
                        {data.fields.price} {"THB/Day"}
                      </Typography>
                    </CardContent>
                  </CardActionArea>
                  <CardActions sx={{ padding: "0px 12px 12px 12px" }}>
                    <Button
                      color="primary"
                      variant="contained"
                      fullWidth
                      sx={{
                        height: "56px",
                        borderRadius: "8px",
                        textTransform: "none",
                        "&:disabled": {
                          backgroundColor: "#93C5FD",
                          color: "#FFFFFF", // Change to your desired disabled text color
                        },
                      }}
                      onClick={() => handleAddtoCart(data)}
                      disabled={isAlreadyInCart}
                    >
                      <Typography
                        sx={{
                          fontFamily: "Inter, sans-serif",
                          fontSize: "16px",
                          fontWeight: 500,
                          lineHeight: "24px",
                        }}
                      >
                        {!isAlreadyInCart ? "Add to cart" : "Added"}
                      </Typography>
                    </Button>
                  </CardActions>
                </Card>
              </Grid>
            );
          })}
        </Grid>
      </Grid>
    </Grid>
  );
};
