import React, { useReducer, ReactNode } from "react";
import { createContext } from "use-context-selector";

// Define the initial state of the cart
interface CartState {
  cartItems: any[];
  total: number;
  discount: number;
  grandTotal: number;
  discountCode: string;
}

interface CartItem {
  id: any;
  name: string;
  price: number;
}

const initialState: CartState = {
  cartItems: [],
  total: 0,
  discount: 0,
  discountCode: "",
  grandTotal: 0,
};

type State = typeof initialState;

type CartAction =
  | { type: "ADD_TO_CART"; payload: CartItem }
  | {
      type: "ADD_Count_OF_CART";
      payload: {
        id: string;
      };
    }
  | {
      type: "DELETE_Count_OF_CART";
      payload: {
        id: string;
      };
    }
  | {
      type: "REMOVE_FROM_CART";
      payload: {
        id: string;
      };
    }
  | {
      type: "SET_TOTAL";
      payload: {
        total: number;
      };
    }
  | {
      type: "SET_GRAND_TOTAL";
      payload: {
        grandTotal: number;
      };
    }
  | {
      type: "SET_DISCOUNT";
      payload: {
        discount: number;
      };
    }
  | {
      type: "SET_DISCOUNT_CODE";
      payload: {
        discountCode: string;
      };
    };

type Dispatch = (action: CartAction) => void;

const cartReducer = (state: CartState, action: CartAction): CartState => {
  switch (action.type) {
    case "ADD_TO_CART":
      return { ...state, cartItems: [...state.cartItems, action.payload] };
    case "ADD_Count_OF_CART":
      return {
        ...state,
        cartItems: state.cartItems.map((item: any) =>
          item.sys.id === action.payload.id
            ? { ...item, count: item.count + 1 }
            : item
        ),
      };
    case "DELETE_Count_OF_CART":
      return {
        ...state,
        cartItems: state.cartItems.map((item: any) =>
          item.sys.id === action.payload.id
            ? { ...item, count: item.count - 1 }
            : item
        ),
      };
    case "REMOVE_FROM_CART":
      return {
        ...state,
        cartItems: state.cartItems.filter(
          (item: any) => item.sys.id !== action.payload.id
        ),
      };
    case "SET_TOTAL":
      return {
        ...state,
        total: action.payload.total,
      };
    case "SET_GRAND_TOTAL":
      return {
        ...state,
        grandTotal: action.payload.grandTotal,
      };
    case "SET_DISCOUNT":
      return {
        ...state,
        discount: action.payload.discount,
      };
    case "SET_DISCOUNT_CODE":
      return {
        ...state,
        discountCode: action.payload.discountCode,
      };
    default:
      return state;
  }
};

export const CartContext = createContext<[State, Dispatch]>([
  initialState,
  () => null,
]);

export const useValue = () => useReducer(cartReducer, initialState);

export const CartProvider = ({ children }: { children: ReactNode }) => {
  return (
    <CartContext.Provider value={useValue()}>{children}</CartContext.Provider>
  );
};
