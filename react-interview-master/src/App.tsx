import { Box } from "@mui/material";
import { AppHead } from "./common/components/app-header";
import { Footer } from "./common/components/app-footer";
import { CartProvider } from "./context/CartContext";
import { Home } from "./page/Home";

import "./App.css";

function App() {
  return (
    <div className="app-container">
      <CartProvider>
        <AppHead />
        <Box
          component="main"
          sx={{
            flexGrow: 1,
            maxHeight: "calc(100vh - 210px)", // Subtract any fixed header height (adjust as needed)
            overflowY: "auto",
            marginTop: "64px",
          }}
        >
          <Home />
        </Box>
        <Footer />
      </CartProvider>
    </div>
  );
}

export default App;
